const functions = require('../node_modules/firebase-functions');
const paytmChecksum = require('../node_modules/paytmchecksum/PaytmChecksum');
const Axios = require('../node_modules/axios');  
const admin = require('../node_modules/firebase-admin');
admin.initializeApp();

export const payWithPaytm = functions.https.onRequest((functionRequest: any, functionResponse: any) => {
    functionResponse.set('Access-Control-Allow-Origin', '*');
    if (functionRequest.method === 'OPTIONS') {
      functionResponse.set('Access-Control-Allow-Methods', 'POST, GET');
      functionResponse.set('Access-Control-Allow-Headers', 'Content-Type');
      functionResponse.set('Access-Control-Max-Age', '3600');
    }
    admin.firestore().collection("setting").doc("setting").get()
        .then((settingDocument: any) => {
            const txnAmount = settingDocument.data()['Client Fee'];
            const callbackURL = settingDocument.data()['Paytm Status URL'];
            const apiHeaders: any = { headers: { 'Content-Type': 'application/json' } }  
            const merchantID: string = 'lklogk83419551396940';
            const merchantKey: string = 'lxuLo%6mYS5%!HT&';
            const website: any = "WEBSTAGING";
            const orderID: any = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) + Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            const customerID: any = "SOM";
            const generateChecksumData = {
                requestType: "Payment",
                mid: merchantID,
                websiteName: website,
                orderId: orderID,
                callbackUrl: callbackURL,
                txnAmount: { value: txnAmount, currency: "INR" },
                userInfo: { custId: customerID },
            };
            const initiateTransactionURL: any = `https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=${merchantID}&orderId=${orderID}`;
            const showPaymentPageURL: any = `https://securegw-stage.paytm.in/theia/api/v1/showPaymentPage?mid=${merchantID}&orderId=${orderID}`;
            paytmChecksum.generateSignature(JSON.stringify(generateChecksumData), merchantKey)
                .then((checksum: any) => { 
                    const initiateTransactionData = {
                        head: { signature: checksum },
                        body: generateChecksumData,
                    }
                    Axios.post(initiateTransactionURL, JSON.stringify(initiateTransactionData), apiHeaders)
                        .then((initiateTransactionResponse: any) => { 
                            functionResponse.writeHead(200, { 'Content-Type': 'text/html' });
                            functionResponse.write('<html>');
                            functionResponse.write('<head>');
                            functionResponse.write('<title>Deals4Inventory Payment Page</title>');
                            functionResponse.write('</head>');
                            functionResponse.write('<body>');
                            functionResponse.write(`<center><h1 style="font-family: 'Trebuchet';" >Please Do Not Refresh This Page...</h1></center>`);
                            functionResponse.write(`<form method="post" action="${showPaymentPageURL}" name="paytm">`);
                            functionResponse.write('<table border="1">');
                            functionResponse.write('<tbody>')
                            functionResponse.write(`<input type="hidden" name="mid" value="${merchantID}">`)
                            functionResponse.write(`<input type="hidden" name="orderId" value="${orderID}">`)
                            functionResponse.write(`<input type="hidden" name="txnToken" value="${initiateTransactionResponse.data.body.txnToken}">`)
                            functionResponse.write('</tbody>')
                            functionResponse.write('</table>');
                            functionResponse.write('<script type="text/javascript">');
                            functionResponse.write('document.paytm.submit();');
                            functionResponse.write('</script>');
                            functionResponse.write('</form>')
                            functionResponse.write('</body>');
                            functionResponse.write('</html>');
                            functionResponse.end();
                        })
                        .catch((error: any) => {
                            console.log(`Initiate Transaction Error: ${error}`)
                            functionResponse.send(`Initiate Transaction Error: ${error}`)
                        })        
                })
                .catch((error: any) => {
                    console.log(`Generate Checksum Error: ${error}`)
                    functionResponse.send(`Generate Checksum Error: ${error}`)
                })
        })
})

export const getPaymentStatus = functions.https.onRequest((functionRequest: any, functionResponse: any) => {
    functionResponse.set('Access-Control-Allow-Origin', '*');
    if (functionRequest.method === 'OPTIONS') {
      functionResponse.set('Access-Control-Allow-Methods', 'POST');
      functionResponse.set('Access-Control-Allow-Headers', 'Content-Type');
      functionResponse.set('Access-Control-Max-Age', '3600');
    }
    var transactionStatus = "";
    if (functionRequest.body.STATUS == "TXN_SUCCESS") {
        transactionStatus = "Success";
    } else {
        transactionStatus = "Failure";
    }
    const days = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    const months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];  
    const today = new Date();
    const day = days[today.getDay()];
    const date = today.getDate();
    const month = months[today.getMonth()];
    const year = today.getFullYear();
    const hour = today.getHours();
    const minute = today.getMinutes();
    admin.firestore().collection('transactions').doc(`${functionRequest.body.ORDERID}`).set({
        orderID: functionRequest.body.ORDERID,
        paymentGateway: "PayTM",
        transactionStatus: transactionStatus,
        date: day + ' ' + month + " " + date + ", " + year + " " + hour + ":" + minute,
        paymentGatewayData: functionRequest.body,
    });
    admin.firestore().collection('setting').doc('setting').get()
        .then((doc: any) => {
            functionResponse.redirect(`${doc.data()['Paytm Redirect URL']}/${functionRequest.body.ORDERID}`);
        })
        .catch((error: any) => {
            console.log(`Get Document Error: ${error}`);
        })
})