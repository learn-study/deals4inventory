npm run-script lint
npm run-script build
firebase serve --only functions

firebase target:apply hosting deve-deals4inventory deve-deals4inventory
firebase deploy --only hosting:deve-deals4inventory

{
  "hosting": {
    "target": "deve-deals4inventory",
    "public": "deals4inventory",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "rewrites": [ {
      "source": "**",
      "destination": "/index.html"
    } ],
    "headers": [
      {
        "source": "**/*.@(js|html)",
        "headers": [
          {
            "key": "Cache-Control",
            "value": "no-store, max-age=0"
          }
        ]
      }
    ]
  }
}

firebase target:apply hosting staging-deals4inventory staging-deals4inventory
firebase deploy --only hosting:staging-deals4inventory

{
  "hosting": {
    "target": "staging-deals4inventory",
    "public": "deals4inventory",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "rewrites": [ {
      "source": "**",
      "destination": "/index.html"
    } ],
    "headers": [
      {
        "source": "**/*.@(js|html)",
        "headers": [
          {
            "key": "Cache-Control",
            "value": "no-store, max-age=0"
          }
        ]
      }
    ]
  }
}

firebase target:apply hosting prod-deals4inventory prod-deals4inventory
firebase deploy --only hosting:prod-deals4inventory

{
  "hosting": {
    "target": "prod-deals4inventory",
    "public": "deals4inventory",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
    "rewrites": [ {
      "source": "**",
      "destination": "/index.html"
    } ],
    "headers": [
      {
        "source": "**/*.@(js|html)",
        "headers": [
          {
            "key": "Cache-Control",
            "value": "no-store, max-age=0"
          }
        ]
      }
    ]
  }
}

Deve Rules

rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if
          request.time < timestamp.date(2021, 1, 24);
    }
  }
}

Prod Rules

rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if false;
    }
  }
}