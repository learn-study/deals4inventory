export interface Dataset {
    firestorePath: string,
    operations: {
        create: boolean,
        update: boolean,
        delete: boolean
    },
    features: {
        reOrderableRows: boolean
    },
    columns: {
        name: string,
        key: string,
        type: string,
        edit: boolean,
        sortable: boolean,
        filterable: boolean,
        dropdown?: {
            options: {}[],
            optionsLabel: string,
            optionsValue: string
        },
        autocomplete?: {
            options: {}[],
            optionsLabel: string,
            filterKey: string
        }
    }[],
    title: string
}