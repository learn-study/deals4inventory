import { DataService } from './../../services/data.service';
import { UserService } from './../../services/user.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Dataset } from './../../models/dataset.model';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'trade-page',
  templateUrl: './trade-page.component.html',
  styleUrls: ['./trade-page.component.scss']
})

export class TradePageComponent implements OnInit, OnDestroy {
  role: {};
  cas: any[];
  tradeDataset: Dataset;
  tradeDataset$ = new BehaviorSubject(null);
  subscription: Subscription = new Subscription();

  constructor(private userService: UserService, private dataService: DataService) { }

  ngOnInit() {
    this.getRole();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getRole() {
    this.subscription.add(
      this.userService.role$.subscribe(role => {
        if (role != null) {
          this.role = role;
          this.getCAS();
        }
      })  
    )
  }

  getCAS() {
    this.subscription.add(
      this.dataService.cas$.subscribe(cas => {
        if (cas != null) {
          this.cas = cas.map(casValue => {
            delete casValue["documentID"];
            return casValue;
          });
          this.setTradeDataset();
        }
      })
    )
  }

  setTradeDataset() {
    if (this.role['tradeRights']) {
      this.tradeDataset = {
        firestorePath: "trade",
        operations: {
          create: true,
          update: true,
          delete: true
        },
        features: {
          reOrderableRows: true
        },
        columns: [
          {name: 'Product Name', key: 'name', type: 'text', edit: true, sortable: true, filterable: true},    
          {name: 'Chemical Name', key: 'chemicalname', type: 'text', edit: true, sortable: true, filterable: true},
          {name: 'CAS', key: 'cas', type: 'specialAutocomplete', edit: true, sortable: true, filterable: true, autocomplete: {options: this.cas, optionsLabel: "name", filterKey: "name"}},
          {name: 'Manafacturer', key: 'mfg', type: 'text', edit: true, sortable: true, filterable: true},
          {name: 'Manafacturer Date', key: 'mfgdate', type: 'date', edit: true, sortable: true, filterable: true},
          {name: 'Expiry Date', key: 'expirydate', type: 'date', edit: true, sortable: true, filterable: true},
          {name: 'Quantity', key: 'quantity', type: 'number', edit: true, sortable: true, filterable: true},
          {name: 'Location', key: 'location', type: 'text', edit: true, sortable: true, filterable: true},
          {name: 'Packing', key: 'packstyle', type: 'text', edit: true, sortable: true, filterable: true},
          {name: 'Price', key: 'price', type: 'currency', edit: true, sortable: true, filterable: true},
          {name: 'Taxes', key: 'taxes', type: 'percentage', edit: true, sortable: true, filterable: true},
          {name: 'Price Basis', key: 'pricebasis', type: 'text', edit: true, sortable: true, filterable: true},
          {name: 'Delivery Schedule', key: 'deliveryschedule', type: 'text', edit: true, sortable: true, filterable: true},
          {name: 'Part Shipments Allowed', key: 'psa', type: 'specialDropdown', edit: true, sortable: true, filterable: true, dropdown: {options: [{name: "Yes"}, {name: "No"}], optionsLabel: "name", optionsValue: "name"}},
          {name: 'Payment Terms', key: 'paymentterms', type: 'textarea', edit: true, sortable: true, filterable: true},
          {name: 'Test Certificates', key: 'certificates', type: 'specialImages', edit: true, sortable: false, filterable: false},
          {name: 'Product Pictures', key: 'pictures', type: 'specialImages', edit: true, sortable: false, filterable: false}
        ],
        title: "Trade"
      }
    } else {
      this.tradeDataset = {
        firestorePath: "trade",
        operations: {
          create: false,
          update: false,
          delete: false
        },
        features: {
          reOrderableRows: true
        },
        columns: [
          {name: 'Product Name', key: 'name', type: 'text', edit: true, sortable: true, filterable: true}, 
          {name: 'Manafacturer', key: 'mfg', type: 'text', edit: true, sortable: true, filterable: true},   
          {name: 'Quantity', key: 'quantity', type: 'number', edit: true, sortable: true, filterable: true},
          {name: 'Location', key: 'location', type: 'text', edit: true, sortable: true, filterable: true},
        ],
        title: "Trade"
      }
    }
    this.tradeDataset$.next(this.tradeDataset);
  }
}
