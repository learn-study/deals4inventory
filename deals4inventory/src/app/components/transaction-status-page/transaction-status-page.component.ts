import { Subscription } from 'rxjs';
import { UserService } from './../../services/user.service';
import { DataService } from '../../services/data.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'transaction-status',
  templateUrl: './transaction-status-page.component.html',
  styleUrls: ['./transaction-status-page.component.scss']
})

export class TransactionStatusComponent implements OnInit, OnDestroy {
  isLoaded: boolean;
  isRestrictedAccess: boolean;
  firstTime: boolean;
  user: {} = {};
  routeParams: {} = {};
  transactionData: {} = {};
  transactionFormWidth: string = "";
  subscription: Subscription = new Subscription();

  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.getUser();
    this.getScreenWidth();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getUser() {
    this.subscription.add(
      this.userService.user$.subscribe(user => {
        if (user) {
          this.user = user;
          this.getRouteParams();
        }
      })  
    )
  }

  getRouteParams() {
    this.subscription.add(
      this.activatedRoute.params.subscribe(params => {
        if (params) {
          this.routeParams = params;
          this.checkUserValidity();
        }
      })
    )
  }

  checkUserValidity() {
    this.subscription.add(
      this.dataService.getFirestoreCollectionWithWhere("user", "transactionOrderID", "==", this.routeParams['orderID']).subscribe(collection => {
        if (collection) {
          var users = collection.map(document => {
            return document.payload.doc.data();
          })
          this.isLoaded = true;
          if (users.length == 0) {
            this.firstTime = true;
            this.dataService.updateFirestoreDocument("user", this.user['uid'], {transactionOrderID: this.routeParams['orderID']});
            this.getTransactionData();
          } else if (users.length != 0 && users[0]['uid'] == this.user['uid']) {
            this.getTransactionData();
          } else {
            this.isRestrictedAccess = true;
            
          }
        }
      })
    )
  }

  getTransactionData() {
    this.subscription.add(
      this.dataService.getFirestoreDocument("transactions", this.routeParams['orderID']).subscribe(document => {
        if (document.payload.data()) {
          this.transactionData = document.payload.data();
          this.checkTransactionStatus();
        }
      })
    )
  }

  checkTransactionStatus() {
    if (this.transactionData['transactionStatus'] == 'Success') {
      if (this.firstTime) {
        this.dataService.updateFirestoreDocument("user", this.user['uid'], { role: "client", membershipType: "Member", membershipDate: this.dataService.getTime(), ...this.dataService.getStorageKey("potentialUserClientInformation")})
        this.dataService.deleteStorageKey("potentialUserClientInformation");
      } 
    }
  }

  getScreenWidth() {
    if (screen.width > 768) {
      this.transactionFormWidth = "50%"
    } else {
      this.transactionFormWidth = "100%"
    }
  }
}
