import { Dataset } from './../../models/dataset.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from './../../services/data.service';
import { UserService } from './../../services/user.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Component({
  selector: 'support-page',
  templateUrl: './support-page.component.html',
  styleUrls: ['./support-page.component.scss']
})

export class SupportPageComponent implements OnInit, OnDestroy {
  isLoaded: boolean;
  
  routeParams: {};
  
  userRoles: {}[] = [
    {role: "guest"},
    {role: "client"},
    {role: "support"},
    {role: "admin"}
  ];
  userRole: {};
  user: {};

  contactDataset: Dataset;
  contactDataset$ = new BehaviorSubject(null);
  
  usersDataset: Dataset;
  usersDataset$ = new BehaviorSubject(null);
  
  casDataset: Dataset;
  casDataset$ = new BehaviorSubject(null);
  
  transactionsDataset: Dataset;
  transactionsDataset$ = new BehaviorSubject(null);

  private subscription: Subscription = new Subscription();

  constructor(private userService: UserService, private dataService: DataService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getUser();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getUser() {
    this.subscription.add(
      this.userService.user$.subscribe(user => {
        if (user != null) {
          this.user = user;
          this.getRole();
        }
      })
    )
  }

  getRole() {
    this.subscription.add(
      this.userService.role$.subscribe(role => {
        this.userRole = role;
        this.getRouteParams();
      })
    )
  }

  getRouteParams() {
    this.subscription.add(
      this.activatedRoute.params.subscribe(params => {
        if (params != null) {
          this.routeParams = params;
          this.setSupportPages();
        }
      })
    )
  }

  setSupportPages() {
    if (this.userRole['viewSupport']) {
      this.setContact();
      this.setCAS();
      this.setUser();
      this.setTransactions();  
    } else {
      this.router.navigate([''])
    }
    this.isLoaded = true;
  }
  
  setContact() {
    this.contactDataset = {
      firestorePath: "contact",
      operations: {
        create: false,
        update: false,
        delete: true
      },
      features: {
        reOrderableRows: true
      },
      columns: [
        {name: 'First Name', key: 'firstname', type: 'text', edit: true, sortable: true, filterable: true},    
        {name: 'Last Name', key: 'lastname', type: 'text', edit: true, sortable: true, filterable: true},
        {name: 'Message', key: 'message', type: 'textarea', edit: true, sortable: true, filterable: true},
        {name: 'Contact Number', key: 'contactnumber', type: 'number', edit: true, sortable: true, filterable: true},
        {name: 'Rating', key: 'rating', type: 'text', edit: true, sortable: true, filterable: true},
      ],
      title: "Contact Messages"
    }
    this.contactDataset$.next(this.contactDataset);
  }

  setCAS() {
    this.casDataset = {
      firestorePath: "cas",
      operations: {
        create: true,
        update: true,
        delete: true
      },
      features: {
        reOrderableRows: true
      },
      columns: [
        {name: 'Name', key: 'name', type: 'text', edit: true, sortable: true, filterable: true},    
        {name: 'Number', key: 'number', type: 'text', edit: true, sortable: true, filterable: true},
      ],
      title: "CAS Details"
    }
    this.casDataset$.next(this.casDataset);
  }

  setUser() {
    this.usersDataset = {
      firestorePath: "user",
      operations: {
        create: false,
        update: false,
        delete: true
      },
      features: {
        reOrderableRows: true
      },
      columns: [
        {name: 'Name', key: 'displayName', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Email', key: 'email', type: 'email', edit: false, sortable: true, filterable: true},
        {name: 'User ID', key: 'uid', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Role', key: 'role', type: 'specialDropdown', edit: false, sortable: true, filterable: true, dropdown: {options: this.userRoles, optionsLabel: "role", optionsValue: "role"}},
        {name: 'Log In', key: 'logintime', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Log Out', key: 'logouttime', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Longitude', key: 'longitude', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Latitude', key: 'latitude', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Transaction Order ID', key: 'transactionOrderID', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Company Name', key: 'companyname', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Proprietor Name', key: 'proprietorname', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Address', key: 'address', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Designation', key: 'designation', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Contact Number', key: 'contactnumber', type: 'number', edit: false, sortable: true, filterable: true},
        {name: 'GST Number', key: 'gstnumber', type: 'text', edit: false, sortable: true, filterable: true}
      ],
      title: "Users"
    }
    this.usersDataset$.next(this.usersDataset);
  }

  setTransactions() {
    this.transactionsDataset = {
      firestorePath: "transactions",
      operations: {
        create: false,
        update: false,
        delete: true
      },
      features: {
        reOrderableRows: true
      },
      columns: [
        {name: 'Date', key: 'date', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Order ID', key: 'orderID', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Payment Gateway', key: 'paymentGateway', type: 'text', edit: false, sortable: true, filterable: true},
        {name: 'Status', key: 'transactionStatus', type: 'text', edit: false, sortable: true, filterable: true},
      ],
      title: "Transactions"
    }
    this.transactionsDataset$.next(this.transactionsDataset);
  }
}
