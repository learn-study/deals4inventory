import { Subscription } from 'rxjs';
import { DataService } from './../../services/data.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.scss']
})

export class SignUpPageComponent implements OnInit, OnDestroy {
  signUpForm: {} = {
    newsletterSubscription: "",
    monthlyPaymentPlan: ""
  };
  paymentPlans: {}[] = [];
  paytmURL: string;
  isLoading: boolean = true;
  subscription: Subscription = new Subscription();

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getPaytmURL();
    this.getPaymentPlans();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getPaytmURL() {
    this.subscription.add(
      this.dataService.getFirestoreDocument("setting", "setting").subscribe(document => {
        this.paytmURL = document.payload.data()["Paytm URL"];
      })
    )
  }

  getPaymentPlans() {
    this.subscription.add(
      this.dataService.settings$.subscribe(settings => {
        if (settings) {
          this.isLoading = false;
          this.paymentPlans = settings["Monthly Payment Plans"].map(plan => {
            return {label: `${plan} Month`, value: plan}
          })
        }
      })
  
    )
  }

  payWithPaytm() {
    this.isLoading = true;
    this.dataService.createStorageKey("potentialUserClientInformation", this.signUpForm);
    window.location.href = this.paytmURL;
  }
}
