import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})

export class HomePageComponent implements OnInit {
  carouselItems: any[] = [
    {
      text: "Sell off the excess raw material, get a great price and de-clutter your warehouse.",
      title: "Sell Unsold Ready Stock",
      image: "https://images.unsplash.com/photo-1598976833830-816830d5b72b?ixid=MXwxMjA3fDB8MHx0b3BpYy1mZWVkfDF8YWV1NnJMLWo2ZXd8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    },
    {
      text: "Instead of dumping the unused equipment in the warehouse, list it and sell it to someone who needs it ...",
      title: "Trade-off Spare Raw Materials",
      image: "https://images.unsplash.com/photo-1526958938731-27f1ccdb1817?ixid=MXwxMjA3fDB8MHx0b3BpYy1mZWVkfDN8YWV1NnJMLWo2ZXd8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    },
    {
      text: "Look around your inventory and notice ready stock.  This unsold product can be put to use and generate cash for you.",
      title: "Dispose Surplus Assets",
      image: "https://images.unsplash.com/photo-1599414275896-93076b7c493c?ixid=MXwxMjA3fDB8MHx0b3BpYy1mZWVkfDZ8YWV1NnJMLWo2ZXd8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    },
    {
      text: "List unused asset in the warehouse.  Sell it off at a great price and recover your losses.",
      title: "Offload Used Process Equipment",
      image: "https://images.unsplash.com/photo-1599414275896-93076b7c493c?ixid=MXwxMjA3fDB8MHx0b3BpYy1mZWVkfDZ8YWV1NnJMLWo2ZXd8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
    },
  ]
  responsiveOptions = [
    {
        breakpoint: '1024px',
        numVisible: 2,
        numScroll: 1
    },
    {
        breakpoint: '768px',
        numVisible: 1,
        numScroll: 1
    }
];

  constructor() { }

  ngOnInit() {
  }
}