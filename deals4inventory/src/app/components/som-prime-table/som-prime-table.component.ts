import { Dataset } from './../../models/dataset.model';
import { BehaviorSubject, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { DataService } from './../../services/data.service';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
@Component({
  selector: 'som-prime-table',
  templateUrl: './som-prime-table.component.html',
  styleUrls: ['./som-prime-table.component.scss']
})

export class SomPrimeTableComponent implements OnInit, OnDestroy {
  @Input() dataset$;
  dataset: Dataset;

  data: any[];
  data$ = new BehaviorSubject(null);

  row: {};

  isLoaded: boolean;
  isShowingDialog: boolean;

  dialogFunction: string;

  toastWidth: string = "50%";

  selectedImages: any[];
  selectedImage: any;
  imageFilePath: string;

  subscription: Subscription = new Subscription();

  constructor(private dataService: DataService, private storage: AngularFireStorage) { }

  ngOnInit() {
    this.subscribeToDataset();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  subscribeToDataset() {
    this.subscription.add(
      this.dataset$.subscribe((dataset: Dataset) => {
        if (dataset) {
          this.dataset = dataset;
          this.getData();  
        }
      })
    )
  }

  getData() {
    this.subscription.add(
      this.dataService.getFirestoreCollection(this.dataset.firestorePath).subscribe(collection => {
        const data = collection.map(document => {
          return {
            documentID: document.payload.doc.id,
            ...document.payload.doc.data() as {}
          }
        })
        this.data$.next(data);
        this.subscribeToData();  
      })
    )
  }

  subscribeToData() {
    this.subscription.add(
      this.data$.subscribe(data => {
        this.data = data;
        this.checkScreenWidth();
      })
    )
  }

  checkScreenWidth() {
    this.subscription.add(
      this.dataService.toastWidth$.subscribe(toastWidth => {
        this.toastWidth = toastWidth;
        this.isLoaded = true;
      })
    )
  }

  addingRow() {
    this.row = {};
    this.checkNullImageValues();
    this.dialogFunction = "ADD";
    this.showDialog();
  }

  addRow() {
    var foundEmpty = false;
    this.dataset.columns.forEach(column => {
      if (!this.row[column.key] && !foundEmpty) {
        foundEmpty = true;
        this.hideDialog();
        this.dataService.sendToastMessage({severity:'warn', sticky: false, summary:'Empty Values', detail:'Please Fill In All Required Input To Successfully Add Data!'})      
      }
    })
    if (!foundEmpty) {
      this.dataService.createFirestoreDocument(this.dataset.firestorePath, this.row);
      this.dataService.sendToastMessage({severity:'success', sticky: false, summary:'Added Row', detail:'Item Was Successfully Added!'})
      this.hideDialog();  
    }
  }

  editingRow(row) {
    this.row = row;
    this.checkNullImageValues();
    this.dialogFunction = "EDIT";
    this.showDialog();
  }

  editRow() {
    const documentID = this.row['documentID'];
    delete this.row['documentID'];
    this.dataService.updateFirestoreDocument(this.dataset.firestorePath, documentID, this.row);
    this.dataService.sendToastMessage({severity:'success', sticky: false, summary:'Updated Row', detail:'Item Was Successfully Updated!'})
    this.hideDialog();
  }

  deleteRow(row) {
    this.dataset.columns.filter(column => column.type == "specialImages").forEach(imageField => {
      this.removeImages(imageField.key, row);
    })
    this.dataService.deleteFirestoreDocument(this.dataset.firestorePath, row['documentID']);
    this.dataService.sendToastMessage({severity:'success', sticky: false, summary:'Deleted Row', detail:'Item Was Successfully Deleted!'})
  }

  showDialog() {
    this.isShowingDialog = true;
  }

  hideDialog() {
    this.isShowingDialog = false;
  }

  checkNullImageValues() {
    this.dataset.columns.forEach(column => {
      if (column.type == "specialImages" && this.row[column.key] == null) {
        this.row[column.key] = [];
      }
    })
  }

  viewImages(images) {
    this.selectedImages = images;
    this.dialogFunction = "IMAGES";
    this.showDialog();
  }

  addImage(imageEvent: any, key) {
    this.selectedImage = imageEvent.target.files[0];
    this.imageFilePath = `trade/${this.selectedImage['name'].split('.')[0]}-${this.dataService.getTime()}:${new Date().getSeconds()}`;
    this.storage.upload(this.imageFilePath, this.selectedImage).snapshotChanges().pipe(
      finalize(() => {
        this.storage.ref(this.imageFilePath).getDownloadURL().subscribe(url => {
          if (this.row[key] == null) {
            this.row[key] = [];
          }
          this.row[key].push({filePath: this.imageFilePath, url: url});
        })  
      })
    ).subscribe();
  }

  removeImages(key, row) {
    row[key].forEach(image => {
      this.storage.ref(image['filePath']).delete().toPromise().then(() => {
        row[key].splice(row[key].indexOf(image), 1);
      });
    })
  }
}
