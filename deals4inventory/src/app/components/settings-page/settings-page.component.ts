import { Subscription } from 'rxjs';
import { MessageService } from 'primeng/api';
import { DataService } from './../../services/data.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss']
})

export class SettingsPageComponent implements OnInit, OnDestroy {
  settings: {};

  settingsKeys: any[];

  toastWidth: string;

  isLoaded: boolean;

  monthlyPaymentPlans: {}[] = [
    {month: 1},
    {month: 2},
    {month: 3},
    {month: 4},
    {month: 5},
    {month: 6},
    {month: 7},
    {month: 8},
    {month: 9},
    {month: 10},
    {month: 11},
    {month: 12}
  ]

  subscription: Subscription = new Subscription();

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getSettings();
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
  getSettings() {
    this.subscription.add(
      this.dataService.settings$.subscribe(settings => {
        if (settings != null) {
          this.settings = settings;
          this.settingsKeys = this.sortArrayAlphabetically(Object.keys(this.settings).map(key => { return key; }))
          this.checkScreenWidth();
        }
      })
    )
  }

  saveSettings() {
    this.settings["Monthly Payment Plans"] = [...new Set(this.settings["Monthly Payment Plans"])]
    this.dataService.updateFirestoreDocument("setting", "setting", this.settings);
    this.dataService.sendToastMessage({severity:'success', sticky: false, summary:'Settings', detail:'Your Settings Were Sucessfully Saved!'})
  }

  checkScreenWidth() {
    this.subscription.add(
      this.dataService.toastWidth$.subscribe(toastWidth => {
        if (toastWidth != null) {
          this.toastWidth = toastWidth;
          this.isLoaded = true;
        }
      })
    )
  }

  sortArrayAlphabetically(array) {
    return array.sort((a, b) => a.localeCompare(b));
  }
}
