import { Subscription } from 'rxjs';
import { DataService } from './../../services/data.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})

export class ProfilePageComponent implements OnInit, OnDestroy {
  userRole: {} = {};
  user: {} = {};
  isLoaded: boolean;
  transactionData: {} = {};
  subscription: Subscription = new Subscription();

  constructor(private userService: UserService, private dataService: DataService) { }

  ngOnInit() {
    this.getRole();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getRole() {
    this.subscription.add(
      this.userService.role$.subscribe(role => {
        if (role) {
          this.userRole = role;
          this.getUser();
        }
      })  
    )
  }

  getUser() {
    this.subscription.add(
      this.userService.user$.subscribe(user => {
        if (user != null) {
          this.user = user;
          this.getTransaction();
        }
      })  
    )
  }

  getTransaction() {
    if (this.user['transactionOrderID']) {
      this.subscription.add(
        this.dataService.getFirestoreDocument("transactions", this.user['transactionOrderID']).subscribe(document => {
          if (document.payload.data()) {
            this.transactionData = document.payload.data();
            this.isLoaded = true;
          } else {
            this.isLoaded = false;
          }
        })
      )
    }
  }
}
