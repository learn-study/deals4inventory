import { SupportPageComponent } from './components/support-page/support-page.component';
import { TradePageComponent } from './components/trade-page/trade-page.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { TransactionStatusComponent } from './components/transaction-status-page/transaction-status-page.component';
import { SignUpPageComponent } from './components/sign-up-page/sign-up-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: "", component: HomePageComponent},
  {path: "sign-up", component: SignUpPageComponent},
  {path: "profile", component: ProfilePageComponent},
  {path: "trade", component: TradePageComponent},
  {path: "transaction-status/:paymentGateway/:orderID", component: TransactionStatusComponent},
  {path: "support/:page", component: SupportPageComponent},
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
