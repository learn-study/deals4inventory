import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { DataService } from './services/data.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  user: {};
  userRole: {};
  isLoaded: boolean;
  isShowingDialog: boolean;
  isShowingProfile: boolean;
  dialogWidth: string;
  profileWidth: string;
  sidebarWidth: string = "100%";
  sidebarProfile: boolean = true;
  contactForm: {} = {}
  transactionData: {} = {};

  constructor(private dataService: DataService, private userService: UserService, public authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.userService.getUserLocation();
    this.dataService.getAllData();
    this.dataService.checkToastScreenWidth();
    this.detectUser();
    this.getUser();
    // this.userService.setUserRoles();
  }
  
  detectUser() {
    if (!this.dataService.getStorageKey("user")) {
      this.dataService.createStorageKey("user", "anonymous");
      this.userService.getUser("anonymous")
    } else if (this.dataService.getStorageKey("user") == "anonymous") {
      this.userService.getUser("anonymous");
    } else {
      this.userService.getUser(this.dataService.getStorageKey("user")["uid"])
    }
  }

  getUser() {
    this.userService.user$.subscribe(user => {
      if (user) {
        this.user = user;
        this.getRole();
      }
    }) 
  }

  getRole() {
    this.userService.role$.subscribe(role => {
      if (role) {
        this.userRole = role;
        this.checkScreenWidth();
      }
    })
  }

  checkScreenWidth() {
    if (screen.width > 768) {
      this.dialogWidth = "75%";
    } else if (screen.width < 1024) {
      this.sidebarProfile = false;
    } else {
      this.dialogWidth = "90%";
    }
    this.getTransaction();
  }

  getTransaction() {
    this.isLoaded = true;
    if (this.user['transactionOrderID']) {
      this.dataService.getFirestoreDocument("transactions", this.user['transactionOrderID']).subscribe(document => {
        if (document.payload.data()) {
          this.transactionData = document.payload.data();
        }
      })
    }
    // if (this.userService.checkUserRoleValidity(this.user)) {
    //   this.dataService.sendToastMessage({severity:'info', life: 5000, summary: 'Monthly Client Plan Ended', detail: 'Your monthly payment plan as a client has ended.  To obtain exclusive access to our web portal again, you must pay for a new plan.'});
    // }
  }

  submitContactForm() { 
    this.contactForm['rating'] += "/5";
    this.dataService.createFirestoreDocument("contact", this.contactForm).then(() => {
      this.contactForm = {};
      this.hideDialog();  
    })
  }

  showDialog() {
    this.isShowingDialog = true;
  }

  hideDialog() {
    this.isShowingDialog = false;
  }

  showProfile() {
    this.isShowingProfile = true;
  }

  hideProfile() {
    this.isShowingProfile = false;
  }
}
