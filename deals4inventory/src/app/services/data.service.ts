import { MessageService } from 'primeng/api';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class DataService {
  sessionStorage: Storage;

  public days = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
  public months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

  public cas$ = new BehaviorSubject(null);
  public cas = [];

  public paytm$ = new BehaviorSubject(null);
  public paytm = [];

  public roles$ = new BehaviorSubject(null);
  public roles = [];

  public settings$ = new BehaviorSubject(null);
  public settings = {};

  public trades$ = new BehaviorSubject(null);
  public trades = [];

  public users$ = new BehaviorSubject(null);
  public users = [];

  public loading$ = new BehaviorSubject(null);

  public toastWidth: string;
  public toastWidth$ = new BehaviorSubject(null);

  constructor (private db: AngularFirestore, private messageService: MessageService) { 
    this.sessionStorage = window.sessionStorage;   
  }

  getAllData() {
    this.getUsers();
    this.getCAS();
    this.getPaytm();
    this.getRoles();
    this.getSettings();
    this.getTrades();
  }

  getCAS() {
    this.getFirestoreCollection("cas").subscribe(collection => {
      this.cas = collection.map(document => {
        return {
          documentID: document.payload.doc.id,
          ...document.payload.doc.data() as {}
        }
      })
      this.cas$.next(this.cas);
    })
  }

  getPaytm() {
    this.getFirestoreCollection("paytm").subscribe(collection => {
      this.paytm = collection.map(document => {
        return {
          documentID: document.payload.doc.id,
          ...document.payload.doc.data() as {}
        }
      })
      this.paytm$.next(this.paytm);
    })
  }

  getRoles() {
    this.getFirestoreCollection("role").subscribe(collection => {
      this.roles = collection.map(document => {
        return {
          documentID: document.payload.doc.id,
          ...document.payload.doc.data() as {}
        }
      })
      this.roles$.next(this.roles);
    })
  }

  getSettings() {
    this.getFirestoreDocument("setting", "setting").subscribe(document => {
      this.settings = document.payload.data();
      this.settings$.next(this.settings);
    })
  }


  getTrades() {
    this.getFirestoreCollection("trade").subscribe(collection => {
      this.trades = collection.map(document => {
        return {
          documentID: document.payload.doc.id,
          ...document.payload.doc.data() as {}
        }
      })
      this.trades$.next(this.trades);
    })
  }


  getUsers() {
    this.getFirestoreCollection("user").subscribe(collection => {
      this.users = collection.map(document => {
        return {
          documentID: document.payload.doc.id,
          ...document.payload.doc.data() as {}
        }
      })
      this.users$.next(this.users);
    })
  }

  checkToastScreenWidth() {
    if (screen.width < 768) {
      this.toastWidth = "90%";
    } else {
      this.toastWidth = "50%";
    }
    this.toastWidth$.next(this.toastWidth);
  }

  sendToastMessage(message: {}) {
    this.messageService.clear();
    this.messageService.add(message);
  }

  getFirestoreCollection(collection: string) {
    return this.db.collection(`${collection}`).snapshotChanges();
  }

  getFirestoreCollectionWithWhere(collection: string, key: any, operation: any, value: any) {
    return this.db.collection(`${collection}`, ref => ref.where(key, operation, value)).snapshotChanges();
  }

  getFirestoreDocument(collection: string, documentID: string) {
    return this.db.collection(`${collection}`).doc(`${documentID}`).snapshotChanges();
  }

  createFirestoreDocumentWithID(collection: string, documentID: string, newDocument: any) {
    return this.db.collection(`${collection}`).doc(`${documentID}`).set(newDocument);
  }

  createFirestoreDocument(collection: string, newDocument: any) {
    return this.db.collection(`${collection}`).add(newDocument);
  }

  updateFirestoreDocument(collection: string, documentID: string, updatedDocument: any) {
    return this.db.collection(`${collection}`).doc(`${documentID}`).update(updatedDocument);
  }

  deleteFirestoreDocument(collection: string, documentID: string) {
    return this.db.collection(`${collection}`).doc(`${documentID}`).delete();
  }

  getStorageKey(key: string): any {
    return JSON.parse(this.sessionStorage.getItem(key));
  }

  createStorageKey(key: string, value: any): any {
    this.sessionStorage.setItem(key, JSON.stringify(value));
  }

  deleteStorageKey(key: string): any {
    this.sessionStorage.removeItem(key);
  }

  getTime() {
    const today = new Date();
    const day = this.days[today.getDay()];
    const date = today.getDate();
    const month = this.months[today.getMonth()];
    const year = today.getFullYear();
    const hour = today.getHours();
    var minute: any = today.getMinutes();
    if (minute.toString().length == 1) {
      minute = "0" + minute;
    }
    return day + ' ' + month + " " + date + ", " + year + " " + hour + ":" + minute;
  }
}