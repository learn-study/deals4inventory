import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  user$ = new BehaviorSubject(null);
  user = {};

  userLocation$ = new BehaviorSubject(null);
  userLocation = {};

  role$ = new BehaviorSubject(null);
  role = {};

  roles: {} = {
    anonymous: {
      viewSignUp: false,
      viewSignIn: true,
      viewTrade: false,
      addTrade: false,
      editTrade: false,
      deleteTrade: false,
      viewSupport: false,
      viewClientInformation: false,
      viewMembershipInformation: false,
      tradeRights: false
    },
    guest: {
      viewSignUp: true,
      viewSignIn: false,
      viewTrade: true,
      addTrade: false,
      editTrade: false,
      deleteTrade: false,
      viewSupport: false,
      viewClientInformation: false,
      viewMembershipInformation: true,
      tradeRights: false
    },
    client: {
      viewSignUp: false,
      viewSignIn: false,
      viewTrade: true,
      addTrade: true,
      editTrade: false,
      deleteTrade: false,
      viewSupport: false,
      viewClientInformation: true,
      viewMembershipInformation: true,
      tradeRights: true
    },
    support: {
      viewSignUp: false,
      viewSignIn: false,
      viewTrade: true,
      addTrade: true,
      editTrade: true,
      deleteTrade: true,
      viewSupport: true,
      viewClientInformation: false,
      viewMembershipInformation: false,
      tradeRights: true
    },
    admin: {
      viewSignUp: false,
      viewSignIn: false,
      viewTrade: true,
      addTrade: true,
      editTrade: true,
      deleteTrade: true,
      viewSupport: true,
      viewClientInformation: false,
      viewMembershipInformation: false,
      tradeRights: true
    }  
  }
  
  subscription: Subscription = new Subscription();

  constructor(private dataService: DataService) { }

  getUserLocation() {
    navigator.geolocation.getCurrentPosition(position => {
      this.userLocation = {latitude: position.coords.latitude, longitude: position.coords.longitude};
      this.userLocation$.next(this.userLocation);
    })
  }

  getUser(userID: string) {
    if (userID != "anonymous") {
      // this.user = this.dataService.getStorageKey("user");
      // this.user$.next(this.user);
      // this.role$.next(this.roles[this.user["role"]]);
      this.dataService.getFirestoreDocument("user", userID).subscribe(document => {
        this.dataService.createStorageKey("user", document.payload.data());
        this.user$.next(document.payload.data())
        this.role$.next(this.roles[document.payload.data()["role"]])
      })      
    } else {
      this.user$.next("anonymous")
      this.role$.next(this.roles["anonymous"])
    }
  }

  checkUserRoleValidity(user) {
    if (this.user != null && this.user["membershipDate"] != null) {
      var userMembershipDate = this.user["membershipDate"];
      var splitUserMembershipDate = userMembershipDate.split(" ")
      var userMembershipMonth = this.dataService.months.indexOf(splitUserMembershipDate[1]) + 1;
      var userMembershipEndDate = "";
      var userMembershipEndMonth = userMembershipMonth + this.user["monthlyPaymentPlan"];
      if (userMembershipEndMonth < 12) {
        splitUserMembershipDate[1] = this.dataService.months[userMembershipEndMonth - 1];
        splitUserMembershipDate.forEach(dateValue => {
          if ([1, 2].includes(splitUserMembershipDate.indexOf(dateValue))) {
            userMembershipEndDate += `${dateValue} `;            
          } else if (splitUserMembershipDate.indexOf(dateValue) == 3) {
            userMembershipEndDate += `${dateValue}`;
          }
        })
      } else {
        userMembershipEndMonth -= 12;
        splitUserMembershipDate[1] = this.dataService.months[userMembershipEndMonth - 1];
        splitUserMembershipDate[3] = Number(splitUserMembershipDate[3]) + 1;
        splitUserMembershipDate.forEach(dateValue => {
          if ([1, 2].includes(splitUserMembershipDate.indexOf(dateValue))) {
            userMembershipEndDate += `${dateValue} `;            
          } else if (splitUserMembershipDate.indexOf(dateValue) == 3) {
            userMembershipEndDate += `${dateValue}`;
          }
        })
      }
      if (this.dataService.getTime().includes(userMembershipEndDate)) {
        return true;
      } else {
        return false;
      }
    }
  }

  setUserRoles() {
    this.setAnonymous();
    this.setGuest();
    this.setClient();
    this.setSupport();
    this.setAdmin();
  }

  setAnonymous() {
    this.dataService.createFirestoreDocumentWithID('role', 'anonymous', this.roles["anonymous"])
  }

  setGuest() {
    this.dataService.createFirestoreDocumentWithID('role', 'guest', this.roles["guest"])
  }

  setClient() {
    this.dataService.createFirestoreDocumentWithID('role', 'client', this.roles["client"])
  }

  setSupport() {
    this.dataService.createFirestoreDocumentWithID('role', 'support', this.roles["support"])
  }

  setAdmin() {
    this.dataService.createFirestoreDocumentWithID('role', 'admin', this.roles["admin"])
  }
}