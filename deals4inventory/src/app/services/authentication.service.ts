import { Subscription } from 'rxjs';
import { UserService } from './user.service';
import { auth } from 'firebase';
import { Router } from '@angular/router';
import { DataService } from './data.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  subscription: Subscription = new Subscription();

  constructor (private authentication: AngularFireAuth, private router: Router, private dataService: DataService, private userService: UserService, private messageService: MessageService) { }

  signIn() { 
    return this.authLogin(new auth.GoogleAuthProvider()); 
  }

  authLogin(provider) {
    return this.authentication.auth.signInWithPopup(provider)
    .then(result => {
      this.setUserData(result.user);
    })
    .catch(error => {
      window.alert(error)
    })
  }

  setUserData(user) {
    var userData = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      membershipType: "Not A Member",
      membershipDate: "Not A Member",
      role: "guest",
      logintime: "",
      latitude: "",
      longitude: ""
    };
    this.userService.userLocation$.subscribe(userLocation => {
      if (userLocation == null) {
        userData = {
          ...userData,
          latitude: "USER DENIED ACCESS",
          longitude: "USER DENIED ACCESS"
        }    
      } else {
        userData = {
          ...userData,
          latitude: this.dataService.getStorageKey("location")["latitude"],
          longitude: this.dataService.getStorageKey("location")["longitude"]
        }    
      }
    })
    this.dataService.getFirestoreDocument('user', user.uid).subscribe(document => {
      if (document.payload.data() == undefined) {
        this.dataService.createFirestoreDocumentWithID('user', userData.uid, userData);
        this.userService.user$.next(userData);
        this.userService.role$.next(this.userService.roles[userData["role"]]);
        this.dataService.createStorageKey('user', userData);
      } else {
        userData = { 
          ...userData,
          ...document.payload.data() as {},
          logintime: `${this.dataService.getTime()}`
        }
        this.dataService.updateFirestoreDocument('user', userData.uid, userData);
        const documentUser = document.payload.data();
        this.userService.user$.next(documentUser);
        this.userService.role$.next(this.userService.roles[documentUser["role"]]);
        this.dataService.createStorageKey('user', userData)
      }
    })  
    this.router.navigate(['']);
  }

  signOut() {
    return this.authentication.auth.signOut().then(() => {
      this.dataService.updateFirestoreDocument('user', this.dataService.getStorageKey('user')["uid"], {'logouttime': `${this.dataService.getTime()}`}).then(() => {
        this.dataService.createStorageKey('user', 'anonymous');
        this.userService.getUser("anonymous")
        this.router.navigate(['']);  
      })
    })
  }
}