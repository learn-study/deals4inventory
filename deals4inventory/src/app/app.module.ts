import { SignUpPageComponent } from './components/sign-up-page/sign-up-page.component';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from 'angularfire2';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { DataService } from './services/data.service';
import { environment } from './../environments/environment.prod';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { TransactionStatusComponent } from './components/transaction-status-page/transaction-status-page.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { SomPrimeTableComponent } from './components/som-prime-table/som-prime-table.component';
import { TradePageComponent } from './components/trade-page/trade-page.component';
import { HttpClientModule } from '@angular/common/http';
import { SupportPageComponent } from './components/support-page/support-page.component';
import { SettingsPageComponent } from './components/settings-page/settings-page.component';

import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { CarouselModule } from 'primeng/carousel';
import { MessageService, ConfirmationService, FilterService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { KnobModule } from 'primeng/knob';
import { RatingModule } from 'primeng/rating';
import { SidebarModule } from 'primeng/sidebar';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { DropdownModule } from 'primeng/dropdown';
import { TooltipModule } from 'primeng/tooltip';
import { CalendarModule } from 'primeng/calendar';
import { FileUploadModule } from 'primeng/fileupload';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MultiSelectModule } from 'primeng/multiselect';
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SignUpPageComponent,
    TransactionStatusComponent,
    ProfilePageComponent,
    SomPrimeTableComponent,
    TradePageComponent,
    SupportPageComponent,
    SettingsPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, ""),
    FormsModule,
    HttpClientModule,
    CardModule,
    ButtonModule,
    DividerModule,
    CarouselModule,
    ToastModule,
    DialogModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    RatingModule,
    KnobModule,
    SidebarModule,
    ProgressSpinnerModule,
    TableModule,
    ToolbarModule,
    DropdownModule,
    TooltipModule,
    CalendarModule,
    FileUploadModule,
    AutoCompleteModule,
    RadioButtonModule,
    MultiSelectModule
  ],
  providers: [
    AngularFirestore,
    AngularFireAuth,
    AngularFireStorage,
    DataService,
    UserService,
    AuthenticationService,
    MessageService,
    ConfirmationService,
    FilterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
